package ru.artem.androidtinkoffschool.charting;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Artem Rudoy
 */
public class PieView extends View {
    private List<DisplayableItem> items = new ArrayList<>();
    private RectF bounds;
    private Paint piePaint;

    public PieView(Context context) {
        this(context, null);
    }

    public PieView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setItems(List<DisplayableItem> items) {
        this.items = items;
        setDisplayedAngles();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        bounds.set(0, 0, MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (DisplayableItem it : items) {
            piePaint.setColor(it.color);
            canvas.drawArc(bounds,
                    360 - it.mEndAngle,
                    it.mEndAngle - it.mStartAngle,
                    true, piePaint);
        }
    }


    private void setDisplayedAngles() {
        float totalValues = 0;
        for (DisplayableItem item : items) {
            totalValues += item.value;
        }
        int currentAngle = 0;
        for (DisplayableItem it : items) {
            it.mStartAngle = currentAngle;
            it.mEndAngle = (int) ((float) currentAngle + it.value * 360.0f / totalValues);
            currentAngle = it.mEndAngle;
        }
    }

    private void init() {
        piePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        piePaint.setStyle(Paint.Style.FILL);
        bounds = new RectF();
    }
}