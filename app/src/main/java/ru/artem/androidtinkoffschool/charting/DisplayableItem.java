package ru.artem.androidtinkoffschool.charting;

/**
 * @author Artem Rudoy
 */
public class DisplayableItem {
    public String label;
    public float value;
    public int color;

    public DisplayableItem(String label, float value, int color) {
        this.label = label;
        this.value = value;
        this.color = color;
    }

    public int mStartAngle;
    public int mEndAngle;
}
