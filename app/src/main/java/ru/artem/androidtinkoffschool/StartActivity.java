package ru.artem.androidtinkoffschool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void toCompoundView(View view) {
        CompoundViewActivity.start(this);
    }

    public void toPieView(View view) {
        PieViewActivity.start(this);
    }

    public void toChartView(View view) {
        PieChartActivity.start(this);
    }
}
