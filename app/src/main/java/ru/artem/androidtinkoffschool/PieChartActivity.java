package ru.artem.androidtinkoffschool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import ru.artem.androidtinkoffschool.charting.PieChart;


public class PieChartActivity extends Activity {

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, PieChartActivity.class));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pie_chart);
        final PieChart pie = findViewById(R.id.Pie);
        pie.addItem("London", 2f, ContextCompat.getColor(this, R.color.seafoam));
        pie.addItem("Moscow", 3.5f, ContextCompat.getColor(this, R.color.chartreuse));
        pie.addItem("New York", 2.5f, ContextCompat.getColor(this, R.color.emerald));
        pie.addItem("Minsk", 3f, ContextCompat.getColor(this, R.color.bluegrass));
        pie.addItem("Madrid", 1f, ContextCompat.getColor(this, R.color.turquoise));
    }
}

