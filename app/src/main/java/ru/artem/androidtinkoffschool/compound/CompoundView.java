package ru.artem.androidtinkoffschool.compound;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.artem.androidtinkoffschool.R;


public class CompoundView extends LinearLayout {

    private ImageView image;
    private TextView title;
    private TextView desc;

    public CompoundView(Context context) {
        this(context, null);
    }

    public CompoundView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CompoundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public void setImage(@DrawableRes int resId) {
        image.setImageResource(resId);
    }

    public void setImage(Drawable drawable) {
        image.setImageDrawable(drawable);
    }

    public String getTitle() {
        return title.getText().toString();
    }

    public void setText(String str) {
        title.setText(str);
    }

    public void setText(@StringRes int res) {
        title.setText(res);
    }

    public String getText() {
        return title.getText().toString();
    }

    public void setHint(String str) {
        desc.setVisibility(View.VISIBLE);
        desc.setText(str);
    }

    protected void init(AttributeSet attrs) {
        setOrientation(LinearLayout.HORIZONTAL);

        LayoutInflater.from(getContext()).inflate(R.layout.compound_view, this);
        image = findViewById(R.id.item_image);
        title = findViewById(R.id.item_title);
        desc = findViewById(R.id.item_description);

        if (attrs != null) {
            final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CompoundView);

            setImage(a.getDrawable(R.styleable.CompoundView_image));
            setText(a.getString(R.styleable.CompoundView_titleText));
            setHint(a.getString(R.styleable.CompoundView_descriptionText));

            processImageSize(a);

            a.recycle();
        }
    }

    private void processImageSize(TypedArray a) {
        int dimensionPixelSize = a.getDimensionPixelSize(R.styleable.CompoundView_iconSize, 0);
        if (dimensionPixelSize > 0) {
            image.getLayoutParams().height = dimensionPixelSize;
            image.getLayoutParams().width = dimensionPixelSize;
        }
    }

}
