package ru.artem.androidtinkoffschool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import ru.artem.androidtinkoffschool.charting.DisplayableItem;
import ru.artem.androidtinkoffschool.charting.PieView;

public class PieViewActivity extends AppCompatActivity {

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, PieViewActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_view);
        List<DisplayableItem> items = new ArrayList<>();
        items.add(new DisplayableItem("Moscow", 2f, ContextCompat.getColor(this, R.color.colorPrimary)));
        items.add(new DisplayableItem("London", 3.5f, ContextCompat.getColor(this, R.color.chartreuse)));
        items.add(new DisplayableItem("Minsk", 2.5f, ContextCompat.getColor(this, R.color.slate)));

        PieView pieView = (PieView) findViewById(R.id.pieView);
        pieView.setItems(items);
    }
}
